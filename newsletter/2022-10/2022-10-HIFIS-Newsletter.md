---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #005aa0}
</style>
<title>Discover the Helmholtz IT Services for Science!</title>
<!--[if gte mso 9]><xml>
<o:OfficeDocumentSettings>
<o:AllowPNG/>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml><![endif]-->
<!--[if !mso]><!-->
<link href="css2.css" rel="stylesheet">
<!--<![endif]-->
<!--[if mso]>
<style>
.fallback-innerfont {
font-family: Arial, Helvetica, sans-serif;
}
.fallback-titlefont {
font-family: Arial, Helvetica, sans-serif;
}
.fallback-subtitlefont {
font-family: Arial, Helvetica, sans-serif;
}
.fallback-buttonfont {
font-family: Arial, Helvetica, sans-serif;
}
.fallback-topfooterfont{
font-family: Arial, Helvetica, sans-serif;
}
.m--content * {
font-family: Arial, Helvetica, sans-serif;
}
</style>
<![endif]-->
<style type="text/css">
body {height:100%!important;margin:0;padding:0;width:100%!important;mso-margin-top-alt:0px;mso-margin-bottom-alt:0px;mso-padding-alt:0px 0px 0px 0px;}
#m--background-table {margin:0;padding:0;width:100%!important;mso-margin-top-alt:0px;mso-margin-bottom-alt:0px;mso-padding-alt:0px 0px 0px 0px;}
table {mso-table-lspace:0pt;mso-table-rspace:0pt;}
table, table th, table td {border-collapse:collapse;}
a, img,a img {border:0;outline:none;text-decoration:none;}
img {-ms-interpolation-mode:bicubic;}
#outlook a {padding:0;}
.ReadMsgBody {width:100%;}
.ExternalClass {width:100%;display:block!important;}
.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass th,.ExternalClass td,.ExternalClass div {line-height: 100%;}
.yshortcuts,.yshortcuts a,.yshortcuts a:link,.yshortcuts a:visited,.yshortcuts a:hover,.yshortcuts a span {color:black;text-decoration:none!important;border-bottom:none!important;background:none!important;}
body,table,td,th,p,a,li,blockquote{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;}
.preheader {display:none;display:none!important;mso-hide:all!important;mso-line-height-rule:exactly;visibility:hidden!important;line-height:0!important; font-size:0!important;opacity:0;color:transparent;height:0;width:0;max-height:0;max-width:0;overflow:hidden;}
@media only screen and (max-width: 580px) {
table[class*=m--hide], th[class*=m--hide], td[class*=m--hide], img[class*=m--hide], p[class*=m--hide], span[class*=m--hide] {display:none!important;}
#m--background-table img {
display:block;
height:auto!important;
line-height:100%;
min-height:1px;
width:100%!important;
}
#m--background-table img.nobreak,
#m--background-table img[class*=nobreak] {display:inline-block;}
.m--base,
.m--header,
.m--footer,
.m--section,
.m--content,
table[class*=m--base],
table[class*=m--header],
table[class*=m--footer],
table[class*=m--section],
table[class*=m--content] {
width:100%!important;
}
.m--base,
table[class*=m--base] {
width:95%!important;
}
.m--base-fullwidth,
.m--base-border,
table[class*=m--base-fullwidth],
table[class*=m--base-border] {
width:100%!important;
}
.m--base-fullwidth .m--section-container,
table[class*=m--base-fullwidth] td[class*=m--section-container] {
padding-left:5px;
padding-right:5px;
}
.m--base-fullwidth .m--section-container-fullwidth,
table[class*=m--base-fullwidth] td[class*=m--section-container-fullwidth] {
padding-left:0;
padding-right:0;
}
.m--base-fullwidth .m--section-container-fullwidth .m--content-container,
table[class*=m--base-fullwidth] td[class*=m--section-container-fullwidth] td[class*=m--content-container] {
padding-left:5px;
padding-right:5px;
}
.m--row-breakable .m--col,
table[class*=m--row-breakable] th[class*=m--col] {
display: block!important;
width:100%!important;
}
}
</style>
<!--[if gte mso 15]>
<style type="text/css">
.m--divider--inner {font-size:2px;line-height:2px;}
</style>
<![endif]-->
</head>
<body yahoo="fix" style="Margin:0;padding:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;background:#e6e6e6;">
<div class="preheader" style="display:none;display:none!important; mso-hide:all!important;visibility:hidden!important;mso-line-height-rule:exactly;line-height:0!important;font-size:0!important;opacity:0;color:transparent;height:0;width:0;max-height:0;max-width:0;overflow:hidden;">
Discover the Helmholtz IT Services for Science!
</div>
<table id="m--background-table" style="background:#e6e6e6;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#e6e6e6" align="center">
<table class="m--header" style="border-collapse:collapse;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="transparent" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<td class="fallback-topfooterfont" style="border-collapse:collapse;color:#999999;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:17px;" align="center">
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/subscribe/hifis-newsletter" target="_blank">Subscribe to HIFIS Newsletter</a> /
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/signoff/hifis-newsletter" target="_blank">Unsubscribe</a> /
<a style="text-decoration: none; color: #999999;" href="{% link newsletter/index.md %}" target="_blank">Archive</a>
<br><br>
</td>
<tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#e6e6e6" align="center">
<table class="m--base" style="border-collapse:separate;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#ffffff" align="center">
<table style="border-collapse:collapse;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--section-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--section" style="border-collapse:collapse;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#ffffff" align="center">
<table style="border-collapse:collapse;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="bottom" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="bottom" align="center">
<table cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="center">
<a href="{% link index.md %}" alt="Link to HIFIS homepage">
<img style="max-width:380px;outline-style:none;text-decoration:none;border:none;font-size:12px;line-height:16px;margin:0;" src="hifis_logo_claim.svg" alt="HIFIS Helmholtz Federated IT Services, Digital Services for Science =E2=80=94 Collaboration made easy." width="380" border="0">
</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 0px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="top" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:0px;" valign="top" align="center">
<table class="m--button" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px; border:2px solid #ffffff;font-size:12px;padding:3px 9px;" bgcolor="#005aa0" align="center">
<a class="fallback-buttonfont" href="{% link newsletter/2022-10/2022-10-HIFIS-Newsletter.md %}" target="_blank" style="font-size:12px;line-height:18px;padding:3px 9px;font-weight:bold;font-family:'Open Sans', Arial, Helvetica, sans-serif;color:#ffffff;text-decoration: none;-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px;display:block;">
<span style="color: #ffffff;"><!--[if mso]>&nbsp;<![endif]-->October 2022<!--[if mso]>&nbsp;<![endif]--></span>
</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:10px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">

<div><br>



<div markdown="1">
{% include_relative raw/00-editorial/editorial.md %}
</div>




</div>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:solid;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row m--row-breakable" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:50%;" width="50%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="center">
<img style="outline-style:none;text-decoration:none;border:none;font-size:12px;line-height:16px;margin:0;" src="fireworks-1758_960_720.jpg" alt="fireforks" width="220" border="0">
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:50%;" width="50%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">



<a style="text-decoration: none; " href="#samstory">Sam's Story, Chapter 2</a><br>
<a style="text-decoration: none; " href="#usecases">Community Use Cases</a><br>
<a style="text-decoration: none; " href="#hifisnews">News from HIFIS</a><br>
<a style="text-decoration: none; " href="#education">News from Education</a><br>
<a style="text-decoration: none; " href="#software">News from Software Technology</a><br>
<a style="text-decoration: none; " href="#cloud">News from Helmholtz Cloud</a><br>
<a style="text-decoration: none; " href="#jobs">Data Science Jobs at Helmholtz</a>



</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>



<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">


<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:solid;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

<img style="margin: 20px 20px 20px 20px;max-width: 40%;" src="{% link assets/img/hifisfor/poster-3.svg %}" alt="Sam being helped by HIFIS consultant" border="0" align="right" >

<span style="font-size:18px" id="samstory"><span style="color:#005aa0"><strong>Sam's Story, Chapter 2</strong></span></span><br>

<div markdown="1">
{% include_relative raw/01-main-story/Sams-story-october-teaser.md %}
</div>


</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>











<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">


<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:solid;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>


<img style="margin: 20px 20px 20px 20px;max-width: 40%;" src="{% link assets/img/posts/2022-08-04-use-case-casus/casus-logo.jpg %}" alt="Logo CASUS" border="0" align="right" >

<span style="font-size:18px" id="usecases"><span style="color:#005aa0"><strong>Community Use Cases</strong></span></span><br>

<div markdown="1">
{% include_relative raw/02-use-cases/uc-casus-ai.md %}
</div>

<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:dotted;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

<img style="margin: 20px 20px 20px 20px;max-width: 30%;" src="{% link assets/img/third-party-logos/DAPHNE4NFDI_logo.png %}" alt="Logo DAPHNE4NFDI" border="0" align="left" />
<div markdown="1">
{% include_relative raw/02-use-cases/uc-daphne.md %}
</div>

<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:dotted;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

<div markdown="1">
{% include_relative raw/02-use-cases/uc-we-still-want.md %}
</div>


</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>






<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">


<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:solid;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>


<img style="margin: 20px 20px 20px 20px;max-width: 40%;" src="{% link assets/img/posts/2022-10-06-hifis-evaluation/HIFIS_Eval.jpg %}" alt="HIFIS Evaluation presentations and board" border="0" align="right" >

<span style="font-size:18px" id="hifisnews"><span style="color:#005aa0"><strong>News from HIFIS</strong></span></span><br>

<div markdown="1">
{% include_relative raw/04-news/news-from-hifis.md %}
</div>


<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:dotted;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

<div style="line-height:24px " id="education"><span style="color:#005aa0"><span style="font-size:16px"><strong>News from Education</strong></span></span></div>


<div markdown="1">
{% include_relative raw/04-news/news-education-teach-courses.md %}
</div>

<img style="margin: 20px 20px 20px 20px;max-width: 40%;" src="{% link assets/img/posts/2022-05-20-save-the-date-summer-academy/Incubator_Summer_Academy_visual_broad.jpg %}" alt="Announcement for Summer Academy" border="0" align="right" >

<div markdown="1">
{% include_relative raw/04-news/news-education-summeracademy.md %}
</div>

<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:dotted;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

<img style="margin: 20px 20px 20px 20px;max-width: 20%;" src="megaphone-3790270_960_720.png" alt="Megaphone" border="0" align="left" >

<div style="line-height:24px " id="software"><span style="color:#005aa0"><span style="font-size:16px"><strong>News from Software Technology</strong></span></span></div>

<div markdown="1">
{% include_relative raw/04-news/news-software.md %}
</div>


<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:dotted;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

<div style="line-height:24px " id="cloud"><span style="color:#005aa0"><span style="font-size:16px"><strong>News from Helmholtz Cloud</strong></span></span></div>

<img style="margin: 20px 20px 20px 20px;max-width: 50%;" src="{% link assets/img/services/Cloud_Portal.jpg %}"  alt="Helmholtz Cloud Portal" border="0" align="right" />
<div markdown="1">
{% include_relative raw/04-news/news-cloud-portal.md %}
</div>

<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:solid;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

<div style="line-height:24px " id="cloud"><span style="color:#005aa0"><span style="font-size:16px"><strong>News from the Helmholtz Incubator</strong></span></span></div>

<img style="margin: 20px 20px 20px 20px;max-width: 50%;" src="Hopin_Expobanner_Medium_Text_1500x750px.jpg"  alt="Helmholtz Career Day Poster" border="0" align="right" />
<div markdown="1">
{% include_relative raw/05-news-incubator/news-incubator.md %}
</div>


<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:solid;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>

</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>




<tr style="line-height:0!important;font-size:0!important;height:0;">
<td style="border-collapse:collapse;line-height:0!important;font-size:0!important;height:0;" height="0"><span style="font-size:0;line-height:0;" id="jobs"></span></td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table class="m--button" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px; border:2px solid #ffffff;font-size:12px;padding:3px 9px;" bgcolor="#005aa0" align="center">
<a class="fallback-buttonfont" href="https://www.helmholtz-hida.de/en/jobs/job-offers/" target="_blank" style="font-size:12px;line-height:18px;padding:3px 9px;font-weight:bold;font-family:'Open Sans', Arial, Helvetica, sans-serif;color:#ffffff;text-decoration: none;-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px;display:block;">
<span style="color: #ffffff;"><!--[if mso]>&nbsp;<![endif]-->Data Science Jobs at Helmholtz<!--[if mso]>&nbsp;<![endif]--></span>
</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">
<div style="text-align:center"><span style="color:#999999">You can find our compulsory information<br>
and data privacy policy</span><span style="color:#005aa0"> </span><a style="text-decoration: none; " href="https://www.desy.de/imprint/index_eng.html"><span style="color:#005aa0">here</span></a><span style="color:#005aa0">.</span></div>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">
<div style="text-align:center"><span style="color:#999999">Imprint<br>
Editorial Responsibility and HIFIS Coordination:<br>
Uwe Jandt<br>
support@hifis.net<br>
uwe.jandt@desy.de<br>
Deutsches Elektronen-Synchrotron DESY<br>
Notkestraße 85<br>
D-22607 Hamburg</span></div>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#e6e6e6" align="center">
<table class="m--footer" style="border-collapse:collapse;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="transparent" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-topfooterfont" style="border-collapse:collapse;color:#999999;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:17px;" align="center">
<br>
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/subscribe/hifis-newsletter" target="_blank">Subscribe to HIFIS Newsletter</a> /
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/signoff/hifis-newsletter" target="_blank">Unsubscribe</a> /
<a style="text-decoration: none; color: #999999;" href="{% link newsletter/index.md %}" target="_blank">Archive</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

</body></html>
