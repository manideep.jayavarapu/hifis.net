---
date: 2020-05-01
title: Tasks in May 2020
service: cloud
---

## First iteration of service selection completed
From June 2019 to May 2020, a service survey throughout all 19 Helmholtz centres has been conducted. 
The target of this survey was to determine which services are desired to be offered as services in HIFIS. 
A second target was to identify which centres are willing to act as service providers for the services to be offered in the Helmholtz Cloud. 
