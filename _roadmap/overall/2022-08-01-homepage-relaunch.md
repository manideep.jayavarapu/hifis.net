---
date: 2022-08-01
title: Tasks in August 2022
service: overall
---

## Relaunch of hifis.net
The hifis.net homepage will be relaunched, with improved and aligned design.
Based on the progress that HIFIS meanwhile made, having implemented and distributed numerous services to the scientific communities,
we will increasingly concentrate on the presentation of scientific use cases.
