---
title: HIFIS Privacy Statements, corresponding to DESY Privacy Statements
title_image: default
layout: default
redirect_to: https://www.desy.de/data_privacy_policy
excerpt:
    HIFIS Privacy Statements, corresponding to DESY Privacy Statements
---
