---
title: Reports
title_image: default
layout: default
excerpt:
    HIFIS Internal Documentation and Reports.
---
## Reports
* [3nd Feedback Report of the Scientific Advisory Board, Aug 2022](https://nubes.helmholtz-berlin.de/s/xHidHaxEBi2M82i/download)
* [Annual report for the year 2021](https://nubes.helmholtz-berlin.de/s/o3f4sN3bpD5oDLm/download)
* [2nd Feedback Report of the Scientific Advisory Board, Aug 2021](https://nubes.helmholtz-berlin.de/s/ndKBRLpfSBLHztf/download)
* [Annual report for the year 2020](https://nubes.helmholtz-berlin.de/s/Y79kf2s3xyiZnX2/download)
* [1st Feedback Report of the Scientific Advisory Board, May 2020](https://nubes.helmholtz-berlin.de/s/jazTy7ZRrwqN7AJ/download)
* [Annual report for the year 2019](https://nubes.helmholtz-berlin.de/s/BKydnntR9edteC2/download)

## Internal Documents
The access to the following documents (drafts) is currently partly restricted to HIFIS members.

#### Presentations
* [Folder with the majority of HIFIS presentations](https://nubes.helmholtz-berlin.de/f/70553648)

#### Organisation
* [HIFIS Stakeholder List](https://nubes.helmholtz-berlin.de/f/41769740)
* [HIFIS Working Groups](https://codebase.helmholtz.cloud/hifis/communication/hifis-structure/-/blob/master/hifis_structure.md) (to be updated)
