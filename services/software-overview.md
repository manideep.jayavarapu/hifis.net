---
title: Software Services
title_image: default
layout: default
author: none
redirect_from:
  - services/software/index_eng.html
  - services/software/index_ger.html
  - services/software
  - mission/cluster-software.html
---

The service portfolio of HIFIS Software is structured into 4 different
components that seamlessly interoperate with each other:
Education & Training, Technology, Consulting and Community Services.

<div class="flex-cards">
{%- assign posts = site.pages | where_exp: "item", "item.path contains 'services/software/'" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post excerpt=true %}
{% endfor -%}
</div>
