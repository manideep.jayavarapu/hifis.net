---
title: Erste Schritte in Python
layout: event
organizers:
  - erxleben
lecturers:
  - erxleben
  - Julia Schwabe
type:   workshop
start:
    date:   "2020-11-04"
end:
    date:   "2020-11-04"
registration_link: https://events.hifis.net/event/29/
location:
    campus: "Online Event"
fully_booked_out: true
registration_period:
    from:   "2020-10-26"
    to:     "2020-11-03"
excerpt:
    "In this workshop, a first introduction into the programming language 
    Python will be given. The course will be held in German."
---

## Audience

This course is suited for learners who are not familiar with programming in 
general or the programming language _Python_ in particular.
Participation is open for everybody.

> Please note that the course will be held in _German_ on this occasion.

## Content

The basic structures of the programming language will be introduced in an 
interactive, hands-on fashion.
Afterwards, the learners will have a grasp on the fundamental steps required 
to create own small-scale programs.

