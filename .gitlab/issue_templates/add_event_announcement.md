/assign @erxleb87
/label ~"Progress::ToDo"
/label ~"Topic::Events"

@software-hifis-net, please process this

# Request to add an Event Announcement
>>>
  This template can be used by people who want to announce an event
  related to the HIFIS Software Cluster.
  This Issue will be resolved by a merge request adding the event.
>>>

## Required Information

### Title
>>>
  A short title for the event
>>>

### Type
>>>
    A short word describing what kind of event this is,
    e.g. "workshop", "seminar", "lecture", "discussion"
>>>

### Organizers
>>>
  A list of team members or associates who are to be contacted for
  organisational questions
>>>

### Instructors
>>>
  A list of team members or associates who hold the workshop and are
  responsible for content and didactics.
>>>

### Timeframe
* Start:    YYYY-MM-DD, hh:mm
* End:      YYYY-MM-DD, hh:mm

# Location
>>>
  Check one. If you are not located on a main campus, please provide the
  address of the sub-campus.
>>>

* [ ] AWI
* [ ] DESY
* [ ] DKFZ
* [ ] DLR
* [ ] FZJ
* [ ] GFZ
* [ ] HIDA
* [ ] HMGU
* [ ] HZB
* [ ] HZDR
* [ ] KIT
* [ ] UFZ

>>>
    Please also fill in the room
>>>

* Room:

### Participants

>>>
    Please add any relevant information about the targeted participants, 
    if required
>>>

* Maximum number of participants:

### Registration Link
>>>
    Provide a link for the registration and a start and end date for the
    registration period.
    The link should include the protocol; `mailto` is also possible.
>>>

* Registration Link:        url.to.registration.page
* Registration open from:   YYYY-MM-DD
* Registration open to:     YYYY-MM-DD

### Content Description
>>>
    What topic is the workshop about?
    Do participants need prior knowledge?
    Should something be prepared before participating?
>>>

### Additional Information
>>>
    Provide additional information
>>>
