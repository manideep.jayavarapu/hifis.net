---
title: "We welcome 10.000 Helmholtz AAI Users!"
title_image: camylla-battani-ABVE1cyT7hk-unsplash.jpg
data: 2022-07-13
authors:
  - jandt
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
  - Helmholtz Cloud
excerpt: >
    By mid July 2022, we reached 10.000 user registrations in the central instance of the Helmholtz AAI.
    A good reason to celebrate the success we achieved!
---

## Helmholtz AAI — seamless access to Helmholtz Cloud Services

<figure>
    <img src="{% link assets/img/posts/2022-07-13-10k-aai-users/helmholtz-aai_blue_font_with_hifis_logo.svg %}" alt="Helmholtz AAI" style="float:right;width:30%;padding:5px 5px 20px 20px">
</figure>

In early 2020, HIFIS implemented a community AAI (Authentication and Authorisation Infrastructure) for all Helmholtz,
thus dubbed "[Helmholtz AAI]({% link aai/index.md %})".

Helmholtz AAI allows to transport and mediate user identity information and authorisation between all participating centres and institutions.
Most prominently, the technology allows users to **log in to remote cloud services with their home username and password**.
Single-Sign On (SSO) is a convenient side effect of this procedure, as a recently accomplished login can be re-used to access multiple services
without the need to re-enter the password.

Moreover, access rights to specific resources can be decentrally configured,
thus **elevating collaboration on shared digital resources to a completely new level**.



### Helmholtz AAI for Helmholtz Cloud Services: Just try!

In parallel, HIFIS established numerous Helmholtz Cloud Services that are
connected to and readily accessible via the AAI. These services are provided by Helmholtz Centres for usage within all Helmholtz
and external collaboration partners -- for details see the specific service descriptions. 

If you haven't tried it yet, there is a solution: **Try it now!**  
Go to our [**Helmholtz Cloud Portal** <i class="fas fa-external-link-alt"></i>](https://helmholtz.cloud/services), and test the services you need;
in case of questions, don't hesitate to [**contact our helpdesk**](mailto:support@hifis.net).

Since there are a few things to consider in such a heterogenous landscape of service providers and using methods,
you are invited to also have a quick look at our
[illustrated tutorial on AAI login]({% link aai/howto.md %}).

### Participants and Users welcome

{:.treat-as-figure}
{:.float-right}
![plot with increasing numbers up to 10 thousand, plus color-coded number of users per centre]({% link assets/img/posts/2022-07-13-10k-aai-users/aai_users_per_centre-plot_cumulated_with_overall.svg %})

The obvious benefits of AAI in combination with useful services
attracted both centres and users to join in increasing numbers: Meanwhile,
16 of the 18 Helmholtz Centres, plus the Head Office, [are connected](https://hifis.net/doc/helmholtz-aai/list-of-connected-organisations/#helmholtz-centres)
with active users being registered in the AAI.
The remaining two centres are expected to follow soon.
Users from elsewhere can also use existing ORCID, Github, or Google accounts to log in (with some restrictions).

The user numbers increased drastically over time, basically from **zero to ten thousand within approximately two years**.
Currently, AAI user accounts grow at a rate of roughly **160 per week**.
All connected Helmholtz centres have users in the AAI, plus roughly 2.000 users that come from non-Helmholtz identity providers.
This showcases the increasing significance of HIFIS services, and Helmholtz AAI specifically, for **collaborators outside Helmholtz**.

Once the full-fledged Helmholtz Cloud Regulations are in place, it is expected that even more users will join us.
Those will be users that are not yet able to put their specific use cases in place with the current status of the Helmholtz Cloud.

### More to come

For the first time, HIFIS fostered practical cross-Helmholtz sharing of digital resources on a large scale, and the Helmholtz AAI
is an obvious cornerstone to that endeavour. This success increasingly yields practical use cases from the 
scientific communities, e.g. on group-based sharing of resources in a both flexible and GDPR compliant manner.
Hence, we are cooperating with standardisation groups such as
[AEGIS](https://aarc-project.eu/about/aegis/) and
[CS3](https://www.cs3community.org/)
to help **put such use cases into reality in
a sustainable and scalable manner**.

The AAI will be extended to facilitate more complex workflows (e.g., service orchestration) and also to **improve user experience**.

## Questions? Comments? Proposals?

If you have any feedback to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
