---
title: "Insights from 3rd HIFIS Teammeeting on 5th/6th July 2022"
title_image: background-cloud.jpg
date: 2022-07-15
authors:
 - holz
 - klotz
layout: blogpost
categories:
  - News
tags:
  - Event
  - Report
excerpt: >
  On July 5th and 6th, our 3rd HIFIS Team Meeting took place at the Helmholtz-Zentrum Berlin in Wannsee. 
---

## From past to future

We gathered at the Helmholtz-Zentrum Berlin (HZB) in Berlin-Wannsee, where Volker Gülzow, the speaker of HIFIS, opened our Team Meeting on Tuesday afternoon. The welcome was complemented by the presentation of the HZB by Thomas Frederking, Administrative Manager of the HZB. We started with a quick catch up of the Status Quo of each HIFIS Cluster. The main focus of the meeting then switched from presentations to group work, thus enabling us to work on some ideas on the "future HIFIS" and create an elevator pitch about what HIFIS would be like in 2024.

This thread was taken up in further group work and we brainstormed about dedicated actions regarding the strategic development of HIFIS services, the coordination & development within Helmholtz and integration & linking to external structures.

## Although it was our 3rd Meeting already, it was only the 2nd one we could meet in-person!

Since the HIFIS team has grown since we last met in-person in 2019, it was a pleasure to get to know the colleagues personally. 37 HIFIS team members and HIFIS friends travelled to Berlin to participate onsite, while 20 team members joined the meeting via video conference. 

## What's next

At the end of September 2022, the evaluation of HIFIS' development in the last three years will take place at DESY in Hamburg. The preparation is going on at full speed and we are excited to present what HIFIS achieved in the last years. And of course, draw a picture of what we are up to in the years to come!
