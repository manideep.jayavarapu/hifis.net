---
title: "Meet data science talents at the career corner on Thursday"
title_image: amy-hirschi-JaoVGh5aJ3E-unsplash.jpg
data: 2022-09-20
authors:
  - "Metzler, Danielle"
layout: blogpost
categories:
  - News
tags:
  - Career
  - Data Science
  - Incubator Summer Academy
  - HIDA
excerpt: >
  Are you looking for talents to fill a data science related position in your group at a Helmholtz Centre? Then join us at our Career Corner to meet motivated data scientists from the Helmholtz Association and our partners on September 22, 2022 from 4-6 PM.
---

## Meet data science talents at the career corner on Thursday
Are you looking for talents to fill a data science related position in your group at a Helmholtz Centre? Then join us at our Career Corner to meet motivated data scientists from the Helmholtz Association and our partners on **September 22, 2022 from 4-6 PM**.

The Career Corner is part of the [Incubator Summer Academy](https://www.helmholtz-hida.de/events/incubator-summer-academy-from-zero-to-hero/). Over 375 researchers from Helmholtz Centres and partners have registered to take data science courses from beginner to advanced level. At the Career Corner, you get the chance to meet them.

Listen in as Helmholtz partners present their openings from 4-5 PM. Meet candidates and network at our hiring desk from 5 PM onwards. It’s easy: Put “HIRING” in your avatar’s name in Gathertown and approach and be approached by candidates. Once your avatars meet, you’ll be connected in a video call. You can meet in private meeting rooms to deepen your conversations. 

### How to get there

[Register here now](https://events.hifis.net/event/398/registrations/452/) and sign up for General: Helmholtz Career Corner. Please choose ‘other’ in the dropdown ‘Current Position’ and specify as Recruiter.

Please be in touch if you have any questions: <hida@helmholtz.de>
