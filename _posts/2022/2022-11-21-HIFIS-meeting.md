---
title: "HIFIS Post Review Meeting"
title_image: chris-montgomery-smgTvepind4-unsplash.jpg
data: 2022-11-21
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Helmholtz Incubator
  - Evaluation
  - Report
excerpt: >
    As we could invite neither all HIFIS team members nor all stakeholders to the main review event, we organised an online Post Review Meeting on Friday, 18 November 2022 to keep everyone informed.
---

## Post Review Meeting for all HIFIS Stakeholders
<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="Screenshot of the online meeting's participants"
src="{% link assets/img/posts/2022-11-21-post-review/participants.jpg %}"
/>
</div>
<div class="text-box" markdown="1">
As we posted [some weeks ago]({% post_url 2022/2022-12-12-evaluation-update %}), HIFIS underwent a thorough review after the first few years of ramp-up phase.
As it was not possible to invite all HIFIS team members and all stakeholders to the main event, we organised an online [Post Review Meeting](https://events.hifis.net/event/565/) on Friday, 18 November 2022.

Volker Gülzow, Coordinator and Spokesperson of HIFIS, gave a warm welcome and thanked everybody involved in the smooth running of the evaluation event.
Uwe Jandt, responsible for the overall management of HIFIS, presented an overview both of the HIFIS achievements and the preliminary (very positive) evaluation results.
The cluster managers added brief presentations from their areas.

Further, we performed a technical demonstration of orchestrated cloud services for a use case that has been set up together with [Helmholtz Imaging](https://helmholtz-imaging.de/).
In the exemplary data presented, intracellular organelles have been visualized and 3D rendered using an automated and distributed pipeline.
The demonstration showcased the interconnectivity of cloud services located at different Helmholtz Centres and the modularity of such pipelines, allowing to exchange or scale up pipeline parts when beneficial.
The confection of similar orchestrated IT services and pipelines will be a major focus of HIFIS in the future.

The decentrality of both Helmholtz and HIFIS is clearly a forte: HIFIS as an infrastructure provider and service broker collects and connects the efforts, capabilities and accomplishments in Research Software Engineering (RSE) and IT services of all Helmholtz Centres.
Thus, we can facilitate further usage and developement as well as bridge the knowledge gap between IT and domain science.
By acting as a catalyst in building trust between the 18 Helmholtz Centres, HIFIS can act as a role model within and beyond the Helmholtz Association, a statement that has been stressed during the evaluation.
</div>
</div>
<div class="clear"></div>

## Baton is passed on to other Incubator platforms and graduate schools
HIFIS is not the only platform within Helmholtz that undergoes a thorough evaluation:
Each of the Helmholtz Incubator plattforms and the six adjoined graduate schools have either already been reviewed in the last weeks or are going to be in 2023.
However, as HIFIS was the very first to undergo this process, we have gathered valuable know-how in the organisation of such an event and of course passed this on towards the other platforms, which are going to be reviewed in Hamburg during 2023, including
[Helmholtz Metadata Collaboration](https://helmholtz-metadaten.de/en) (HMC), 
[Helmholtz Imaging](https://helmholtz-imaging.de/) and 
[Data Science in Hamburg](https://www.dashh.org/) (DASHH), the Helmholtz Graduate School for the Structure of Matter.

## Questions? Comments? Suggestions?
If you have any feedback to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
