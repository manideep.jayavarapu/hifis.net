---
title: "Mass Storage for Machine Learning in Seismology"
title_image: globe.jpeg
data: 2022-07-15
authors:
  - klaffki
  - "Münchmeyer, Jannes"
layout: blogpost
categories:
  - Use-Case
tags:
  - DESY Storage (HDF)
  - Helmholtz AI
  - Machine Learning
  - Seismology
redirect_from:
  - use%20case/2022/07/15/use-case-dCache
excerpt: >
    The DESY Storage (HDF) provides seismologists with a repository to share data on earthquakes via SeisBench, a toolbox for machine learning in seismology.
---

## From dCache to DESY Storage (HDF) — HIFIS mass storage from DESY for Helmholtz

<figure>
    <img src="{% link assets/img/posts/2022-06-29-use-case-dCache/dCache-logo.svg %}" alt="Raven as logo for dCache" style="float:right;width:15%;min-width:120px;padding:5px 5px 20px 20px;">
</figure>
[dCache](https://www.dcache.org) is an open source project which developed a system for storing and retrieving large amounts of data, providing world-wide access. 
It has been built and is further developed by Deutsches Elektronen-Synchrotron ([DESY](https://desy.de)),
the Fermi National Accelerator Laboratory ([FNAL](http://www.fnal.gov/)) 
and the Nordic e-Infrastructure collaboration ([NeIC](https://neic.no/nt1/)). 

Thus, the system was a perfect candidate for DESY to provide mass storage Helmholtz-wide via HIFIS.
Actually, it has been one of the first services connected to the Helmholtz AAI in early 2020 as a demonstrator.
Now, it's becoming a regular service and will be available via the Helmholtz Cloud Portal in autumn 2022, branded as "DESY Storage (HDF)", HDF being short for Helmholtz Data Federation.


## SeisBench — A toolbox for machine learning in seismology
The first users of the then prototype are seismologists from the REPORT-DL project:
[Rapid Earthquake Phase Analysis of Ocean-bottom, Regional and Teleseismic events with Deep Learning](https://www.geomar.de/en/fb4-gdy/projects/translate-to-english-report-dl).
This was funded in 2019 by [Helmholtz AI](https://www.helmholtz.ai/themenmenue/you-helmholtz-ai/funding-lines/funded-projects/index.html), another Helmholtz Incubator plattform, and within this context, [SeisBench](https://github.com/seisbench/seisbench) was developed
— A toolbox for machine learning in seismology.

<figure>
    <img src="{% link assets/img/posts/2022-06-29-use-case-dCache/seisbench_logo.svg %}" alt="seisBench logo" style="float:left;width:25%;padding:20px 20px 20px 5px;">
</figure>
SeisBench is an open-source python toolbox, aiming to standardise access to datasets and models for seismic waveform processing with deep learning.
This way, SeisBench both reduces the overhead for developers of such models and bridges the gap between model developers and seismic practitioners. 

Key part of SeisBench is the ability to directly access benchmark datasets and pretrained models. 
To facilitate the sharing of this data, they use the DESY Storage (HDF). 
This service equips them with a high-performance repository, enabling the comfortable sharing of datasets of several hundred gigabytes. 
Additional functionality provided through webDAV allows to implement convenience functions, 
such as the possibility to enumerate available model weights. 
More detailed information can be found in the project's [documentation](https://seisbench.readthedocs.io/en/stable/).

<figure>
    <img src="{% link assets/img/posts/2022-06-29-use-case-dCache/schematic_fig_seisbench.svg %}" alt="Schematic diagram of SeisBench." style="float:right;width:100%;">
    <figcaption>Schematic diagram of SeisBench. By Jack Woollam, license: GPLv3</figcaption>
</figure>

Within the nine months since publication, SeisBench has grown an active user base of almost 200 users. 
These users access the DESY Storage (HDF) repository around 5000 times per month. Users are located internationally, 
including researchers at world-leading institutions (e.g. Harvard, Cambridge, Cornell). 
The majority of users come from outside the Helmholtz community, which highlights the importance to grant world-wide and easy access to such contents. 

In addition to the infrastructure, DESY Storage (HDF) offers the SeisBench team detailed statistics on usage patterns. 
This allows them to identify which parts of their software are most used by the community, e.g., 
which models are of the largest interest. They use this information for planning future focuses in the development of SeisBench.


## How to use DESY Storage (HDF) via HIFIS for _your_ projects
The storage service is usable for any user group with central Helmholtz stakeholders, but not intended for single users. 
Users shall please briefly apply via HIFIS support, providing 
a main contact (if multiple users are involved), the purpose of usage (brief description), including approximate ressources needed, 
the number of users (approximately), the Helmholtz centres / other organisations of the user(s) and the envisioned time frame of usage
in order to set up the service optimally.

## Get in contact
For dCache / DESY Storage (HDF): [Christian Voss](mailto:christian.voss@desy.de?subject="desy-storage"), [Paul Millar](mailto:paul.millar@desy.de?subject="desy-storage")

For SeisBench: [Jannes Münchmeyer](mailto:munchmej@gfz-potsdam.de?subject="SeisBench"), [Jack Woollam](mailto:jack.woollam@kit.edu?subject="SeisBench"), [Andreas Rietbrock](mailto:andreas.rietbrock@kit.edu?subject="SeisBench")

For HIFIS: [HIFIS Support](mailto:support@hifis.net?subject="desy-storage")

---

#### Changelog
- 2022-08-02 -- adapt the date for release of DESY Storage (HDF) in the Helmholtz Cloud Portal
