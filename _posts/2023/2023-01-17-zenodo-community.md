---
title: "HIFIS has a Zenodo Community"
title_image: jonny-gios-SqjhKY9877M-unsplash.jpg
data: 2023-01-17
authors:
 - klaffki
layout: blogpost
categories:
 - News
tags:
 - Open Access
 - Repository
excerpt: >
   HIFIS-related documents on the Zenodo repository can now be included in our Zenodo Community. Please add yours!
---

## What is a Zenodo Community? 
[Zenodo](https://zenodo.org/) is a repository for research across all disciplines. 
It is hosted by CERN's Data Centre which underpins its reliability and sustainability.
After registration, all kinds of documents — from single-file research papers to complex datasets, (research) software etc., can be uploaded and 
will be published with a persistent [Digital Object Identifier](https://www.doi.org/), (DOI), which makes the document findable, identifiable and citeable
even when the physical location of the upload might change.
If your work already has a DOI, you can register this one.
While access can be restricted on Zenodo, it is a strong advocate for open access and with an upload on Zenodo, you can fulfill any open access claims you might need to consider.
This might come in handy if you lack access to an institutional repository.
Besides, you support the open science idea.

These documents can be complemented by metadata such as keywords, and they can be included in one or several [Zenodo Communities](https://zenodo.org/communities).
Such a community is **a collection of documents** on Zenodo and has **a certain topic** which they all belong to. 
Every user can set up a community and, as its curator, is then able to accept or reject documents other user want to include in it.

## What are the benefits for HIFIS users?
HIFIS has set up [its own Zenodo Community](https://zenodo.org/communities/hifis) only recently and we hope it will grow further. 
We see several benefits for our users:

- If you are new to Zenodo or self-publishing your work, this is a good starting point into the world of repositories and persistent identifiers for research documents.
- If you already upload your research on Zenodo, including it in our Community can help to disseminate it further.
- If you are interested in HIFIS-related publications, you can harvest the metadata of all documents within a community via an [OAI-PMH interface](https://www.openarchives.org/pmh/).

##  How do I contribute and show my project in there?
In addition to the benefits mentioned above, you can actively support HIFIS with your contribution to our Zenodo Community.
We foster the open science movement and empower researchers and software engineers to publish their work, e.g. on Zenodo.
But we also need to demonstrate that this is a necessity and we can only do that with YOU.

To add your work to the HIFIS Community is really simple: 
- When you upload the document, there is a tab "Communities" at the metadata section.
You can start typing "HIFIS" and select the Community from a menu.
- If you already uploaded relevant documents to Zenodo, you can add the community retroactively with an edit of the document's metadata section.
- In both cases our curator will be notified and checks whether your work is **about HIFIS** (e.g. a report) or has been done while **using HIFIS** in some way (e.g. some code for which you requested help from HIFIS Consulting or a paper from a research project that uses HIFIS Services).
Both types of contribution are welcome to our Community!
- After the confirmation, your work will appear on the [HIFIS Community page](https://zenodo.org/communities/hifis).

## Questions? Comments? Suggestions?
If you have any feedback to tell us, don't hesitate to [contact us](mailto:support@hifis.net).

